package com.ithillel.lesson06;

import java.util.Date;

public class ClassDemo extends ClassDemoParent {

    { System.out.println("Block..."); }

    static { System.out.println("Static Block... 1"); }

    static { System.out.println("Static Block... 2"); }

    //состояние
    private String name;

    //способы создания объектов

    public ClassDemo(String name) {
        super(name);
        this.name = name;
    }

    public ClassDemo() {
        super("Hello");
        System.out.println("Constructor...");
    }

    //поведение

    public static void main(String[] args) {
        ClassDemo cd = new ClassDemo();
    }
}
