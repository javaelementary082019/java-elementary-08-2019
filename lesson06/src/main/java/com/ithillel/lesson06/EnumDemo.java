package com.ithillel.lesson06;

public class EnumDemo {
    public static void main(String[] args) {

        Level level = Level.HIGH;

        System.out.println(level);

        countLevel("SSSSS");
        countLevel(Level.HIGH);

        Gender gender = Gender.FEMALE;

        System.out.println(gender.getCode());
        System.out.println(gender.getPrettyCode());


    }

    static void countLevel(String level){
        //do smth
    }

    static void countLevel(Level level){
        //do smth
    }
}

enum Level {
    LOW,MEDIUM,HIGH
}

enum Gender {
   FEMALE("f"),
    MALE("m");

    private String code;

    Gender(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getPrettyCode() {
        return "*** " + code + " ****";
    }
}