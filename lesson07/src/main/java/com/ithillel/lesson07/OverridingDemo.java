package com.ithillel.lesson07;

import java.io.IOException;

public class OverridingDemo {
}

class Parent {
    void doSmth(int count) {
        System.out.println("Parent implementation");
    }
}

//1. The method in the child class must have the same signature as the method in the parent class.
class Child extends Parent {

    @Override
    void doSmth(int count) {
        System.out.println("Child implementation");
    }

//    @Override
//    void doSmth(String str) {
//        System.out.println("Child implementation");
//    }
}

//2. The method in the child class must be at least as accessible or more accessible than the method in the parent class.
class Figure {
    protected int calculateSmth() {
        return 101;
    }
}

class Circle extends Figure {

    @Override
    protected int calculateSmth() {
        return 202;
    }

//    @Override
//    public int calculateSmth(){
//        return 202;
//    }

//    @Override
//    int calculateSmth(){
//        return 202;
//    }
}

//3. The method in the child class may not throw a checked exception that is new or broader than the class of any exception thrown in the parent class method.
class Reader {
    public void readSmth() throws IOException {
    }
}

class MyException extends Exception {
}

class CustomReader extends Reader {

    @Override
    public void readSmth() throws IOException {
    }

//    @Override
//    public void readSmth() throws Exception { }

//    @Override
//    public void readSmth() throws FileNotFoundException { }

//    @Override
//    public void readSmth() { }

//    @Override
//    public void readSmth() throws MyException { }

//    @Override
//    public void readSmth() throws RuntimeException{ }
}

//4. If the method returns a value, it must be the same or a subclass of the method in the parent class, known as covariant return types.
class Phone {
    public Number findSmth(){
        return null;
    }
}

class MobilePhone extends Phone{

    @Override
    public Number findSmth(){
        return null;
    }

//    @Override
//    public Integer findSmth(){
//        return null;
//    }

//    @Override
//    public String findSmth(){
//        return null;
//    }

}