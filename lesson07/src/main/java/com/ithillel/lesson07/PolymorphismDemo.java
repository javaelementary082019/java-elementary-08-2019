package com.ithillel.lesson07;

import lombok.AllArgsConstructor;
import lombok.ToString;

import java.util.Arrays;
import java.util.List;

public class PolymorphismDemo {
    public static void main(String[] args) {

        Swim human = new Human("John", 25);
        Swim fish = new Fish("Nemo");
        Swim boat = new Boat(25);

        List<Swim> swimmers = Arrays.asList(human, fish, boat);
        for (Swim s : swimmers) {
            s.swim();
        }
    }
}

interface Swim {
    void swim();
}

@AllArgsConstructor
@ToString
class Human implements Swim {
    private String name;
    private int age;

    @Override
    public void swim() {
        System.out.println(this.toString() + " I swim in the pool... and I'm cool!");
    }

}

@AllArgsConstructor
class Fish implements Swim {
    private String name;

    @Override
    public void swim() {
        System.out.println("I am fish " + name + ". I swim in the ocean...");

    }
}

@AllArgsConstructor
class Boat implements Swim {
    private int speed;

    @Override
    public void swim() {
        System.out.println("I am a boat. My speed is " + speed + " km/h.");
    }
}
