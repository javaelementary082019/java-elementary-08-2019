package com.ithillel.lesson07;

public class Outer {

    private String outerStr = "outer var";
    public static String staticOuterStr = "static outer var";

    public void outerMethod() {
        System.out.println("Outer class method");
        System.out.println(Nested.staticNestedStr);
    }

    class Inner extends OuterParent {
        String innerStr = "inner var";
//        static String staticInnerStr = "static inner var";

        public void innerMethod() {
            System.out.println("Inner class method");
            System.out.println(outerStr);
            System.out.println(staticOuterStr);

        }
    }

    static class Nested extends OuterParent {
        static String staticNestedStr = "static nested var";

        void nestedMethod() {
            System.out.println("Nested class method");
            System.out.println(staticOuterStr);
//            System.out.println(outerStr);
        }
    }

    void outerMethodWithLocalClass() {
        class MethodLocal extends OuterParent{
            void innerMethod() {
                System.out.println("Method-local inner class method");
            }
        }
        MethodLocal ml = new MethodLocal();
        ml.innerMethod();
    }

    public static void main(String[] args) {
        Outer.Inner inner = new Outer().new Inner();
        inner.innerMethod();
        System.out.println(inner.innerStr);

        Outer.Nested nested = new Outer.Nested();
        nested.nestedMethod();
        System.out.println(Nested.staticNestedStr);

        Outer outer = new Outer();
        outer.outerMethodWithLocalClass();

        Hello hello = new Hello() {
            @Override
            public void show() {
                System.out.println("anonymous hello");
            }
        };
        hello.show();
    }
}

interface Hello {
    void show();
}

class OuterParent {
}