package com.ithillel.lesson07;

public interface InterfaceJ8Demo {
    public static void main(String[] args) {
        Formula formula = new Formula() {
            @Override
            public double calculate(int a) {
                return sqrt(a * 100);
            }

//            @Override
//            public double sqrt(int a) {
//                return 111;
//            }
        };

        System.out.println(formula.calculate(100));
        System.out.println(formula.sqrt(16));
        Formula.staticMethod();
    }
}

interface Formula {
    double calculate(int a);

    default double sqrt(int a) {
        return Math.sqrt(a);
    }

    static void staticMethod(){
        System.out.println("Interface static method");
    }
}