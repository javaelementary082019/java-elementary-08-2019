package com.ithillel.lesson07.homework;

import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
public class Person {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String city;
}
