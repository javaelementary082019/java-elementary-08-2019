package com.ithillel.lesson07.homework;

public class DemoHW {

    public static void main(String[] args) {

        Person person = Person.builder()
                .id(10L)
                .firstName("John")
                .lastName("Smith")
                .email("jo@gmail.com")
                .city("Kyiv")
                .build();

        System.out.println(person);
//        Person(id=10, firstName=John, lastName=Smith, email=jo@gmail.com, city=Kyiv)

//        MyPerson myPerson = MyPerson.builder()
//                .id(11L)
//                .firstName("John1")
//                .lastName("Smith1")
//                .email("jo1@gmail.com")
//                .city("NY")
//                .build();
//        System.out.println(myPerson);
//        MyPerson(id=11, firstName=John1, lastName=Smith1, email=jo1@gmail.com, city=NY)

//        MyPerson myDefaultPerson = MyPerson.builder()
//                .build();
//
//        System.out.println(person);
//        Person(id=999, firstName=unknown, lastName=unknown, email=unknown, city=unknown)
    }
}
