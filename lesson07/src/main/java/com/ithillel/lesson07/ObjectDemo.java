package com.ithillel.lesson07;

public class ObjectDemo {

    public static void main(String[] args) {

        MyObj myObj = new MyObj("one");
        MyObj anotherObj = new MyObj("one");

        System.out.println(myObj.getClass().getCanonicalName());
        System.out.println(myObj);
        System.out.println(myObj.hashCode());
        System.out.println(anotherObj.hashCode());
        System.out.println(myObj.equals(anotherObj));

//        MyObj clonObj;
//        try {
//            clonObj = (MyObj) myObj.clone();
//            System.out.println(clonObj);
//        } catch (CloneNotSupportedException e) {
//            e.printStackTrace();
//        }

    }


}

class MyObj extends Object {
    private String value;

    public MyObj(String value) {
        this.value = value;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        MyObj myObj = (MyObj) o;
//
//        return value.equals(myObj.value);
//    }
//
//    @Override
//    public int hashCode() {
//        return value.hashCode();
//    }
//
//    @Override
//    public String toString() {
//        return "MyObj{" +
//                "value='" + value + '\'' +
//                '}';
//    }
//
//    @Override
//    protected Object clone() throws CloneNotSupportedException {
//        return super.clone();
//    }
}
